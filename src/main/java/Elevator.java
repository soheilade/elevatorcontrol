import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Elevator implements Runnable {
private int id;
private int currentFloor;
private List<Task> tasks;
private int direction;
public static Logger logger = LoggerFactory.getLogger(Elevator.class);

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}
public int getCurrentFloor() {
	return currentFloor;
}
public void setCurrentFloor(int currentFloor) {
	this.currentFloor = currentFloor;
}
public List<Task> getTasks() {
	return tasks;
}
public void setTasks(List<Task> goals) {
	this.tasks = goals;
}
public Elevator(int id, int currentFloor){
	this.id=id;
	this.currentFloor=currentFloor;
	tasks=new ArrayList<Task>();
}
public boolean addTask(Task t){
	return this.tasks.add(t);
}
public void getStatus(){
	
}


//master control system compute the cost of assigning a task to this particular elevator with this function
public int computeCost(Task t) {
	//factors that affect the cost: 
	//1)the cost of going to that floor 
	//2) the cost of travel
	//3) the number of existing tasks for elevator
	int cost = /*Math.abs(currentFloor-t.getSource()) + Math.abs((t.getTarget()-t.getSource()))+*/tasks.size();
	logger.info("elevator "+id+" is in "+currentFloor+" cost for task "+ t +" is "+cost);
	return cost;
}
public int getDirection() {
	return direction;
}
public void setDirection(int direction) {
	this.direction = direction;
}

	public void run() {
		while (true) {
			if(!tasks.isEmpty()){
			int minCost = ControlSystem.maxFloors;
			Task minTask = null;
			for (Task t : tasks) {
				int cost = Math.abs(currentFloor - t.getSource());
				if (cost < minCost) {
					minCost = cost;
					minTask = t;
				}
			}
			
			currentFloor = minTask.getTarget();
			tasks.remove(minTask);
			
			logger.info("task "+minTask+" executed by "+this.id+". i'm in "+currentFloor);
			}
		}
	}
}
