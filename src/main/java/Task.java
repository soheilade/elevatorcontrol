
public class Task {
private	int id=0;
private int source;
private int target;
public int getId(){
	return id;
}
public int getSource() {
	return source;
}
public void setSource(int source) {
	this.source = source;
}
public int getTarget() {
	return target;
}
public void setTarget(int target) {
	this.target = target;
}
public Task(int s, int t){
	id++;
	source=s;
	target=t;
}
public String toString(){
	return source+"->"+target;
}
public boolean equals(Task m){
    if(this.source == m.source && this.target == m.target)
         return true;
    return false;
  }
}
