import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/*
 * in this control system people arrive at each floor and register their requests as tasks
 * then the control system has to schedual tasks among its agents
 */
public class ControlSystem {
private List<Elevator> agents;
public static int maxFloors=50;
public static Logger logger = LoggerFactory.getLogger(ControlSystem.class);

public ControlSystem(List<Elevator> elevators, List<Task> myTasks ){
	
	this.agents=elevators;
	for(Elevator e:agents){
		
		final Thread t = new Thread((Runnable) e);
		logger.info("starting Elevator "+e.getId());
		t.start();
	}
	while(!myTasks.isEmpty()){
		Task t=myTasks.get(0);
		logger.info("scheduling task"+t);
		if(schedualComingTask(t)) 
			myTasks.remove(t);	
	}
}
//get the status of Elevator
public Elevator getStatus(int eId){
	for(Elevator e : agents){
		if (e.getId()==eId)
			return e;
	}
	logger.info("no elevator with id "+eId);
	return null;
}
//update can happen in two ways: reset the tasks or add a task to its task list or remove a task
	public void resetElevator(int eId){
		getStatus(eId).getTasks().clear();
	}
	//receiving a pickup request is actually updating by adding a task
	public boolean addTask(Task t){
		return schedualComingTask(t);
	}
	public boolean removeTask(int eId,Task t){
		return getStatus(eId).getTasks().remove(t);
	}
//
//schedualing
	public boolean schedualComingTask(Task t){
		
		int minimum=maxFloors;
		int minId=0;
		for(Elevator e : agents){
			int cost=e.computeCost(t);
			if(cost<minimum) {
				minimum=cost;
				minId=e.getId();
			}
		}
		logger.info("task "+t+" assigned to Elevator"+ minId);
		return getStatus(minId).addTask(t);
	}
	//print the status of all elevators
	public void printStateOfElevators() {
		for(int i=0;i<agents.size();i++){
			Elevator curElevator = agents.get(i);
			System.out.println("Elevator " +i+ " is at "+ curElevator.getCurrentFloor());
		}
		
	}
	public static void main(String[] args){
		//I only assume there is two elevators
		List<Elevator> elevators=new ArrayList<Elevator>(Arrays.asList(new Elevator(0, 0),new Elevator(1, 0)));
		//we assume the order of tasks are in the order they are inserted into the list, it can be improved by adding a time stamp to the task though!
		List<Task> myTasks=new ArrayList<Task>(Arrays.asList(new Task(0, 6),new Task(5, 7),new Task(3,6),new Task(7,2),new Task(4,8)));
		
		ControlSystem master= new ControlSystem(elevators, myTasks);
		master.printStateOfElevators();//printing the current status of elevators
		master.addTask(new Task(2, 7));//recieving a pick up request
		master.addTask(new Task(8, 9));//recieving another pick up request
	}
	
	
}
